package com.userservice.UserService.Controller;

import com.userservice.UserService.DTO.UserDto;
import com.userservice.UserService.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController{
    @Autowired
    private UserService userService;

    //get all user service data
    @GetMapping("/getAll")
    public List<UserDto> getAllUser(){
        return userService.getAllusers();
    }

    //Create new user
    @PostMapping("/createUser")
    public  boolean createUser(@RequestBody UserDto user){
        return userService.createUser(user);
    }
    //Update user service data
    @PutMapping("/updateUser/{id}")
    public boolean updateUser(@RequestBody UserDto user, @PathVariable final Long id){
        return  userService.updateUser(user,id);
    }
}
