package com.userservice.UserService.Services;

import com.userservice.UserService.DTO.UserDto;
import com.userservice.UserService.Repository.UserRepository;
import com.userservice.UserService.Entities.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository repository;

    @Autowired
    private  RestTemplateBuilder restTemplate;

    //get All user data
    public List<UserDto> getAllusers() {
        List<UserDto> users = null;
        try {
            users = repository.findAll()
                    .stream()
                    .map(userEntity -> new UserDto(
                            userEntity.getId(),
                            userEntity.getName(),
                            userEntity.getAge()
                    )).collect(Collectors.toList());

        } catch (Exception ex) {
            LOGGER.warn("Exception in user service" + ex);
        }
        return users;
    }
    //create new user
    public boolean createUser(UserDto user){
        try{
            UserEntity userEntity=new UserEntity();
            userEntity.setId(user.getId());
            userEntity.setName(user.getName());
            userEntity.setAge(user.getAge());
            repository.save(userEntity);
            return true;
        }catch(Exception ex){
            LOGGER.warn("User service error"+ex);
        }
        return false;
    }

    //Update user records
    public boolean updateUser(UserDto user, Long id){
        try{
            UserEntity userEntity=new UserEntity();
            userEntity.setName(user.getName());
            userEntity.setAge(user.getAge());
            userEntity.setId(id);
            repository.save(userEntity);
            return true;
        }catch(Exception ex){
            LOGGER.warn("User service error:"+ex);
        }
        return false;
    }



}